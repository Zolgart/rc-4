import './appStyles.scss';
import Header from "./components/Header/Header";
import Home from "./pages/Home/Home";
import { Route, Routes } from "react-router-dom";
import Cart from "./pages/Cart/Cart";
import Favorite from "./pages/Favorite/Favorite";
import Modal from './components/Modal/Modal';

function App() {

  return (

    <div className="app">
      <Header />
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/cart" element={<Cart />}/>
        <Route path="/favorite" element={<Favorite />}/>
      </Routes>
      <Modal text = 'Ви впевнені, що хочете додати цей товар в корзину?'  />
    </div>
  );
}

export default App;
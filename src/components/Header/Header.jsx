import styles from "./Header.module.scss"
import {ReactComponent as Star} from '../../img/13487794321580594410.svg'
import {ReactComponent as Cart} from '../../img/7524495561543238919.svg'
import {ReactComponent as Home} from '../../img/1976053.svg'
import {Link} from "react-router-dom";
import { useSelector } from "react-redux"


function Header(){
    const favoriteCaunter = useSelector((state) => state.favorites.length);
    const cardCaunter = useSelector((state) => state.cart.length);

    const itemFavorite = useSelector((state) => state.favorites);
    localStorage.setItem('Favorite Items', JSON.stringify( itemFavorite));

    const itemCart = useSelector((state) => state.cart);
    localStorage.setItem('Cart Items', JSON.stringify(itemCart));


    return (
        <div className={styles.header}>
            <Link to='/'>
                <h1 className={styles.title}>Нумізматична продукція</h1>
            </Link>
            <div className={styles.iconWrapper}>
            <div className={styles.wrapper}>
                <Link to='/' className={styles.link}><Home className={styles.home}/></Link>
                </div>
                <div className={styles.wrapper}>
                    <p className={styles.card__caunter}>{cardCaunter}</p>
                    <Link to='/cart' className={styles.link}><Cart className={styles.card}/></Link>
                </div>

                <div className={styles.wrapper}>
                    <p className={styles.favorite__caunter}>{favoriteCaunter}</p>
                    <Link to='/favorite' className={styles.link}><Star className={styles.favorite}/></Link>
                </div>
            </div>
        </div>
    )
}

export default Header;